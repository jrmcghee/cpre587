-------------------------------------------------------------------------
-- Matthew Dwyer
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- piped_mac.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a basic piplined axi-stream mac unit. It
-- multiplies two integer/Q values togeather and accumulates them.
--
-- NOTES:
-- 10/25/21 by MPD::Inital template creation
-------------------------------------------------------------------------

library work;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity piped_mac is
  generic(
      -- Parameters of mac
      C_DATA_WIDTH : integer := 32
    );
	port (
        ACLK	: in	std_logic;
		ARESETN	: in	std_logic;       

        -- AXIS slave data interface
		SD_AXIS_TREADY	: out	std_logic;
		SD_AXIS_TDATA	: in	std_logic_vector(C_DATA_WIDTH*2-1 downto 0);  -- Packed data input
		SD_AXIS_TLAST	: in	std_logic;
        SD_AXIS_TUSER   : in    std_logic;  -- Should we treat this first value in the stream as an inital accumulate value?
		SD_AXIS_TVALID	: in	std_logic;
        SD_AXIS_TID     : in    std_logic_vector(7 downto 0);

        -- AXIS master accumulate result out interface
		MO_AXIS_TVALID	: out	std_logic;
		MO_AXIS_TDATA	: out	std_logic_vector(C_DATA_WIDTH-1 downto 0);
		MO_AXIS_TLAST	: out	std_logic;
		MO_AXIS_TREADY	: in	std_logic;
		MO_AXIS_TID     : out   std_logic_vector(7 downto 0)
    );

attribute SIGIS : string; 
attribute SIGIS of ACLK : signal is "Clk"; 

end piped_mac;


architecture behavioral of piped_mac is
    -- Internal Signals
	
	
	-- Mac stages
    type PIPE_STAGES is (TEMP_STAGE0);
    
    type Stage1_rec is record -- Read stage
        A   : unsigned(C_DATA_WIDTH-1 downto 0);
        B : unsigned(C_DATA_WIDTH-1 downto 0);
        tid  : std_logic_vector(7 downto 0);
        tlast : std_logic;
        tuser : std_logic;
        tvalid : std_logic;
    end record Stage1_rec;
    
    constant DEFAULT_STAGE1 : Stage1_rec := (
        A   => (others => '0'),
        B => (others => '0'),
        tid  => (others => '0'),
        tlast => '0',
        tuser => '0',
        tvalid => '0'
    );
    
    type Stage2_rec is record -- Multiply stage
        C   : unsigned(2*C_DATA_WIDTH-1 downto 0);
        tid  : std_logic_vector(7 downto 0);
        tlast : std_logic;
        tuser : std_logic;
        tvalid : std_logic;
    end record Stage2_rec;
    
    constant DEFAULT_STAGE2 : Stage2_rec := (
        C   => (others => '0'),
        tid  => (others => '0'),
        tlast => '0',
        tuser => '0',
        tvalid => '0'
    );
    
    type Stage3_rec is record -- Accumulate stage
        C   : unsigned(C_DATA_WIDTH-1 downto 0);
        tid  : std_logic_vector(7 downto 0);
        tlast : std_logic;
        tuser : std_logic;
        tvalid : std_logic;
    end record Stage3_rec;
    
    constant DEFAULT_STAGE3: Stage3_rec := (
        C   => (others => '0'),
        tid  => (others => '0'),
        tlast => '0',
        tuser => '0',
        tvalid => '0'
    );
    
    type Stage4_rec is record -- write stage
        C   : unsigned(C_DATA_WIDTH-1 downto 0);
        tid  : std_logic_vector(7 downto 0);
        tlast : std_logic;
        tuser : std_logic;
        tvalid : std_logic;
    end record Stage4_rec;
    
    constant DEFAULT_STAGE4: Stage4_rec := (
        C   => (others => '0'),
        tid  => (others => '0'),
        tlast => '0',
        tuser => '0',
        tvalid => '0'
    );

	
	-- Debug signals, make sure we aren't going crazy
    signal mac_debug : std_logic_vector(31 downto 0);
    
    signal st_1 : Stage1_rec;
    signal st_2 : Stage2_rec;
    signal st_3 : Stage3_rec;
    signal st_4 : Stage4_rec;
    
    signal stalled : std_logic;

begin

    -- Interface signals


    -- Internal signals
    
    -- Pipelined, always ready
    SD_AXIS_TREADY <= not stalled;
	
	
	-- Debug Signals
    mac_debug <= x"00000000";  -- Double checking sanity
   
   process (ACLK) is
   begin 
    if rising_edge(ACLK) then  -- Rising Edge

      -- Reset values if reset is low
      if ARESETN = '0' then  -- Reset
        st_1 <= DEFAULT_STAGE1;
        st_2 <= DEFAULT_STAGE2;
        st_3 <= DEFAULT_STAGE3;
        st_4 <= DEFAULT_STAGE4;
        stalled <= '0';
		
      else
      
         --- Stage 4 Write Back, AXI handshake
         if( st_3.tvalid = '1') then         
             MO_AXIS_TVALID <= st_3.tvalid;
             MO_AXIS_TDATA <= std_logic_vector(st_3.C);
             MO_AXIS_TLAST <= st_3.tlast;
             MO_AXIS_TID <= st_3.tid;
             stalled <= not MO_AXIS_TREADY;
        end if;

         --- Stage 3 Accumulate
         if( (st_2.tvalid = '1') and (stalled = '0')) then
             if(st_2.tuser = '1') then
                st_3.C <= st_2.C(((C_DATA_WIDTH*2 - C_DATA_WIDTH/2) -1) downto C_DATA_WIDTH/2);
             else
                st_3.C <= st_2.C(((C_DATA_WIDTH*2 - C_DATA_WIDTH/2) -1) downto C_DATA_WIDTH/2) + st_3.C;
             end if;
             
            st_3.tid <= st_2.tid;
            st_3.tlast <= st_2.tlast;
            st_3.tuser <= st_2.tuser;
        end if;
        
        st_3.tvalid <= st_2.tvalid;
      
         --- Stage 2 Multiply
         if((st_1.tvalid = '1') and (stalled = '0')) then
            st_2.C <= st_1.A * st_1.B;
            st_2.tid <= st_1.tid;
            st_2.tlast <= st_1.tlast;
            st_2.tuser <= st_1.tuser;
        end if;      
        
        st_2.tvalid <= st_1.tvalid;
      
        if((SD_AXIS_TVALID = '1') and (stalled = '0')) then
             --- Stage 1 Read
            st_1.A <= unsigned(SD_AXIS_TDATA(C_DATA_WIDTH*2-1 downto C_DATA_WIDTH));
            st_1.B <= unsigned(SD_AXIS_TDATA(C_DATA_WIDTH-1 downto 0));
            st_1.tid <= SD_AXIS_TID;
            st_1.tlast <= SD_AXIS_TLAST;
            st_1.tuser <= SD_AXIS_TUSER;
        end if;  
        st_1.tvalid <= SD_AXIS_TVALID;
        
      end if;  -- Reset

    end if;  -- Rising Edge
   end process;
end architecture behavioral;
