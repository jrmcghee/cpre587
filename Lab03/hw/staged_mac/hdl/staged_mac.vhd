-------------------------------------------------------------------------
-- Matthew Dwyer
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- staged_mac.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a basic staged axi-stream mac unit. It
-- multiplies two integer/Q values togeather and accumulates them.
--
-- NOTES:
-- 10/25/21 by MPD::Inital template creation
-------------------------------------------------------------------------

library work;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity staged_mac is
  generic(
      -- Parameters of mac
      C_DATA_WIDTH : integer := 32
    );
	port (
        ACLK	: in	std_logic;
		ARESETN	: in	std_logic;       

        -- AXIS slave data interface
		SD_AXIS_TREADY	: out	std_logic;
		SD_AXIS_TDATA	: in	std_logic_vector(C_DATA_WIDTH*2-1 downto 0);  -- Packed data input
		SD_AXIS_TLAST	: in	std_logic;
        SD_AXIS_TUSER   : in    std_logic;  -- Should we treat this first value in the stream as an inital accumulate value?
		SD_AXIS_TVALID	: in	std_logic;
        SD_AXIS_TID     : in    std_logic_vector(7 downto 0);

        -- AXIS master accumulate result out interface
		MO_AXIS_TVALID	: out	std_logic;
		MO_AXIS_TDATA	: out	std_logic_vector(C_DATA_WIDTH-1 downto 0);
		MO_AXIS_TLAST	: out	std_logic;
		MO_AXIS_TREADY	: in	std_logic;
		MO_AXIS_TID     : out   std_logic_vector(7 downto 0)
    );

attribute SIGIS : string; 
attribute SIGIS of ACLK : signal is "Clk"; 

end staged_mac;


architecture behavioral of staged_mac is
    -- Internal Signals
	
	
	-- Mac state
    type STATE_TYPE is (WAIT_FOR_VALUES, MAC, WRITE);
    signal state : STATE_TYPE;
    
    signal acc_reg : unsigned(C_DATA_WIDTH*2 -1 downto 0);
    signal first_input : std_logic;
    
    signal m1 : unsigned(C_DATA_WIDTH-1 downto 0);
    signal m2 : unsigned(C_DATA_WIDTH-1 downto 0);
    
    signal sid : std_logic_vector(7 downto 0);
    signal t_last : std_logic;
    
	
	-- Debug signals, make sure we aren't going crazy
    signal mac_debug : std_logic_vector(31 downto 0);

begin

    -- Interface signals


    -- Internal signals
	MO_AXIS_TDATA <= std_logic_vector(acc_reg(((C_DATA_WIDTH*2 - C_DATA_WIDTH/2) -1) downto C_DATA_WIDTH/2));
	  
	
	-- Debug Signals
    mac_debug <= x"00000000";  -- Double checking sanity
   
   process (ACLK) is
   begin 
    if rising_edge(ACLK) then  -- Rising Edge

      -- Reset values if reset is low
      if ARESETN = '0' then  -- Reset
        state       <= WAIT_FOR_VALUES;

      else
        case state is  -- State
            -- Wait here until we receive values
            when WAIT_FOR_VALUES =>
            
                SD_AXIS_TREADY <= '1';
            
                if(SD_AXIS_TUSER = '1') then 
                    acc_reg <= (others => '0');
                end if;
                
            
                -- Wait here until we recieve valid values
                
                if(SD_AXIS_TVALID = '1') then
                        sid <= SD_AXIS_TID;
                        t_last <= SD_AXIS_TLAST;
                        m1 <= unsigned(SD_AXIS_TDATA(C_DATA_WIDTH*2-1 downto C_DATA_WIDTH));
                        m2 <= unsigned(SD_AXIS_TDATA(C_DATA_WIDTH-1 downto 0));
                        state <= MAC;
                end if;
                
            
               
            when MAC =>
            
                SD_AXIS_TREADY <= '0';
            
                acc_reg <= acc_reg + m1 * m2;
                state <= WRITE;
            
            when WRITE =>
                
                 MO_AXIS_TVALID <= '1';
                 MO_AXIS_TID <= sid;
                 MO_AXIS_TLAST <= t_last;
                 
			     if(MO_AXIS_TREADY = '1') then
			         state <= WAIT_FOR_VALUES;
			     end if;
			
			-- Other stages go here	
			
            when others =>
                state <= WAIT_FOR_VALUES;
                -- Not really important, this case should never happen
                -- Needed for proper synthisis         
        end case;  -- State
      end if;  -- Reset

    end if;  -- Rising Edge
   end process;
end architecture behavioral;
