#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2020.1 (64-bit)
#
# Filename    : elaborate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for elaborating the compiled design
#
# Generated by Vivado on Fri Sep 22 12:56:28 CDT 2023
# SW Build 2902540 on Wed May 27 19:54:35 MDT 2020
#
# Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
#
# usage: elaborate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xelab -wto c620fce02ad54673aa14af8746b26629 --incr --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot mac_tb_behav xil_defaultlib.mac_tb -log elaborate.log"
xelab -wto c620fce02ad54673aa14af8746b26629 --incr --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot mac_tb_behav xil_defaultlib.mac_tb -log elaborate.log

