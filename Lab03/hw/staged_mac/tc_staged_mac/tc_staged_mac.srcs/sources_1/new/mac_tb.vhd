-------------------------------------------------------------------------
-- Matthew Dwyer
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- staged_mac.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a basic staged axi-stream mac unit. It
-- multiplies two integer/Q values togeather and accumulates them.
--
-- NOTES:
-- 10/25/21 by MPD::Inital template creation
-------------------------------------------------------------------------

library work;
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mac_tb is
  generic(
      -- Parameters of mac
      C_DATA_WIDTH : integer := 32
    );
	port ( ACLK	: in	std_logic;
		ARESETN	: in	std_logic   );



end mac_tb;


architecture behavioral of mac_tb is
    -- Internal Signals
    
    component staged_mac is
  generic(
      -- Parameters of mac
      C_DATA_WIDTH : integer := 32
    );
	port (
        ACLK	: in	std_logic;
		ARESETN	: in	std_logic;       

        -- AXIS slave data interface
		SD_AXIS_TREADY	: out	std_logic;
		SD_AXIS_TDATA	: in	std_logic_vector(C_DATA_WIDTH*2-1 downto 0);  -- Packed data input
		SD_AXIS_TLAST	: in	std_logic;
        SD_AXIS_TUSER   : in    std_logic;  -- Should we treat this first value in the stream as an inital accumulate value?
		SD_AXIS_TVALID	: in	std_logic;
        SD_AXIS_TID     : in    std_logic_vector(7 downto 0);

        -- AXIS master accumulate result out interface
		MO_AXIS_TVALID	: out	std_logic;
		MO_AXIS_TDATA	: out	std_logic_vector(C_DATA_WIDTH-1 downto 0);
		MO_AXIS_TLAST	: out	std_logic;
		MO_AXIS_TREADY	: in	std_logic;
		MO_AXIS_TID     : out   std_logic_vector(7 downto 0)
    );


    end component;
    
    signal SD_AXIS_TREADY	: 	std_logic;
    signal SD_AXIS_TDATA	: 	std_logic_vector(C_DATA_WIDTH-1 downto 0);  -- Packed data input
    signal SD_AXIS_TLAST	: 	std_logic;
    signal SD_AXIS_TUSER   :     std_logic;  -- Should we treat this first value in the stream as an inital accumulate value?
    signal SD_AXIS_TVALID	: 	std_logic;
    signal SD_AXIS_TID     :     std_logic_vector(7 downto 0);

    -- AXIS master accumulate result out interface
    signal MO_AXIS_TVALID	: 	std_logic;
    signal MO_AXIS_TDATA	: 	std_logic_vector(C_DATA_WIDTH*2-1 downto 0);
    signal MO_AXIS_TLAST	: 	std_logic;
    signal MO_AXIS_TREADY	: 	std_logic;
    signal MO_AXIS_TID     :    std_logic_vector(7 downto 0);
    signal MO_AXIS_TUSER    : std_logic;
	
	
	-- Mac state
    type STATE_TYPE is (WRITE_VALUE, VERIFY_VALUE, WAIT_READ);
    signal state : STATE_TYPE;
    
    signal cnt : integer;
    
    type TEST_MEM is array(natural range <>) of std_logic_vector(C_DATA_WIDTH*2 -1 downto 0);
    signal mem : TEST_MEM(5 downto 0) := ( 0 => x"0004000000040000",
                                           1 => x"0004000000080000",
                                           2 => x"0008000000080000",
                                           3 => x"0004000000008000",
                                           4 => x"0004000000004000",
                                           5 => x"0008000000002000");
                                           
    signal last : std_logic;
    signal first : std_logic := '1';
    

begin

    DUT : staged_mac
    port map(
        ACLK	=>ACLK,
		ARESETN	=> ARESETN,

        -- AXIS slave data interface
		SD_AXIS_TREADY	=> MO_AXIS_TREADY,
		SD_AXIS_TDATA => MO_AXIS_TDATA,  -- Packed data input
		SD_AXIS_TLAST	=> MO_AXIS_TLAST, --: in	std_logic;
        SD_AXIS_TUSER   => MO_AXIS_TUSER, --: in    std_logic;  -- Should we treat this first value in the stream as an inital accumulate value?
		SD_AXIS_TVALID	=> MO_AXIS_TVALID,
		SD_AXIS_TID     => MO_AXIS_TID,

        -- AXIS master accumulate result out interface
		MO_AXIS_TVALID	=> SD_AXIS_TVALID,
		MO_AXIS_TDATA	=> SD_AXIS_TDATA,
		MO_AXIS_TLAST	=> SD_AXIS_TLAST,
		MO_AXIS_TREADY	=> SD_AXIS_TREADY,
		MO_AXIS_TID     => open
		);

   process (ACLK) is
   begin 
    if rising_edge(ACLK) then  -- Rising Edge

      -- Reset values if reset is low
      if ARESETN = '0' then  -- Reset
        state       <= WRITE_VALUE;
        cnt <= 0;
        SD_AXIS_TREADY <= '0';

      else
      
        if(cnt = 5) then
            last <= '1';
        else
            last <= '0';
        end if;
        
        if(cnt = 0) then
            first <= '1';
        else
            first <= '0';
        end if;
        
        
        
      
        case state is  -- State
            -- Wait here until we receive values
            when WRITE_VALUE =>
            
            SD_AXIS_TREADY <= '0';
            
            if(MO_AXIS_TREADY = '1') then
                MO_AXIS_TVALID <= '1';
                MO_AXIS_TDATA <= mem(cnt);
                MO_AXIS_TLAST <= last;
                MO_AXIS_TID <= "01010101";
                MO_AXIS_TUSER <= first;
                state <= VERIFY_VALUE;
                cnt <= cnt + 1;
            end if;
            
           
               
            when VERIFY_VALUE =>
            
            SD_AXIS_TREADY <= '1';
            MO_AXIS_TVALID <= '0';
            MO_AXIS_TUSER <= '0';
            
            if(SD_AXIS_TVALID = '1') then
                state <= WRITE_VALUE;
            end if;
         
			
            when others =>
                state <= WRITE_VALUE;
                -- Not really important, this case should never happen
                -- Needed for proper synthisis         
        end case;  -- State
      end if;  -- Reset

    end if;  -- Rising Edge
   end process;
end architecture behavioral;
