add_force {/mac_tb/ACLK} -radix hex {0 0ns} {1 5000ps} -repeat_every 10000ps
add_force {/mac_tb/ARESETN} -radix hex {0 0ns}
run 20 ns
add_force {/mac_tb/ARESETN} -radix hex {1 0ns}
run 500 ns
