#include "Flatten.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void FlattenLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    //Assumptions

    // int N = 1; // N corresponds to batch size

    // Assuming dims is setup as

    //
    

    // logInfo("Flatten");

    Array1D_fp32 out = this->getOutputData().getData<Array1D_fp32>();
    LayerParams p_out = this->getOutputParams();

    Array3D_fp32 in = dataIn.getData<Array3D_fp32>(); //Assuming input shape
    LayerParams p_in = dataIn.getParams();

    for(long unsigned int i = 0; i < p_in.dims[0]; i++){
       for(long unsigned int j = 0; j < p_in.dims[1]; j++){
            for(long unsigned int k = 0; k < p_in.dims[2]; k++){
                out[i * (p_in.dims[1] * p_in.dims[2]) + j * p_in.dims[2] + k] = in[i][j][k];
            } 
        } 
    }
}

// Compute the convolution using threads
void FlattenLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void FlattenLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void FlattenLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML