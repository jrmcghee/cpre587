#include "Convolutional.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void ConvolutionalLayer::computeNaive(const LayerData& dataIn) const {
    // logInfo("Convolutional");

    Array3D_fp32 out = this->getOutputData().getData<Array3D_fp32>();
    LayerParams p_out = this->getOutputParams();

    Array3D_fp32 in = dataIn.getData<Array3D_fp32>();
    LayerParams p_in = dataIn.getParams();

    Array1D_fp32 b = this->getBiasData().getData<Array1D_fp32>();
    LayerParams p_b = this->getBiasParams();

    Array4D_fp32 w = this->getWeightData().getData<Array4D_fp32>();
    LayerParams p_w = this->getWeightParams();

    int U = 1; // Step size or stride
    int P = (p_in.dims[0] - p_w.dims[0] + U) / U;  // Updated formulas for calculating
    int Q = (p_in.dims[1] - p_w.dims[1] + U) / U;  // output dimensions based on input and filter

    for(long unsigned int m = 0; m < p_w.dims[3]; m++){
        for(int p = 0; p < P; p++){
            for(int q = 0; q < Q; q++){

                fp32 sum = 0;
                for(long unsigned int c = 0; c < p_w.dims[2]; c++){
                    for(long unsigned int r = 0; r < p_w.dims[0]; r++){
                        for(long unsigned int s = 0; s < p_w.dims[1]; s++){
                            sum += in[U*p + r][U*q + s][c] * w[r][s][c][m];  // Changed s and r
                        }
                    }
                }
                sum += b[m];
                out[p][q][(int)m] = sum < 0 ? 0 : sum;  // Changed p and q to match the original dimensions
            }
        }
    }
}


// Compute the convolution using threads
void ConvolutionalLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void ConvolutionalLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void ConvolutionalLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML