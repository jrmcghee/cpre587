#include "MaxPooling.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void MaxPoolingLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    // logInfo("Max Pool");

    int U = 2;

    Array3D_fp32 out = this->getOutputData().getData<Array3D_fp32>();
    LayerParams p_out = this->getOutputParams();

    Array3D_fp32 in = dataIn.getData<Array3D_fp32>(); //Assuming input shape
    LayerParams p_in = dataIn.getParams();

    // printf("%d\n", (int)p_out.dims[0]);
    // printf("%d\n", (int)p_out.dims[1]);
    // printf("%d\n", (int)p_out.dims[2]);

    for(long unsigned int c = 0; c < p_out.dims[2]; c++){
        for(unsigned int p = 0; p < p_out.dims[1];p++){
            for(unsigned int q = 0; q < p_out.dims[0]; q++){

                // printf("Comparing -----\n");
                // printf("X: %d Y: %d\n", U*q, U*p);
                // printf("X: %d Y: %d\n", U*q, U*p +1);
                // printf("X: %d Y: %d\n", U*q + 1, U*p);
                // printf("X: %d Y: %d\n", U*q + 1, U*p +1);

                fp32 m = in[U*q][U*p][c];
                m = m > in[U*q][U*p +1][c] ? m : in[U*q][U*p +1][c];
                m = m > in[U*q + 1][U*p][c] ? m : in[U*q + 1][U*p][c];
                m = m > in[U*q + 1][U*p +1][c] ? m : in[U*q + 1][U*p +1][c];

                // for(int stride_x = U*p; stride_x < U*(((signed)p)+1); stride_x++){
                //     for(int stride_y = U*q; stride_y < U*(((signed)q)+1); stride_y++){
                //         // m = max(m, in[c][stride_x][stride_y]);
                //         m = m > in[c][stride_x][stride_y] ? m : in[c][stride_x][stride_y];
                //     }
                // }

                // printf("Storing at %d %d\n", q, p);
                out[q][p][c] = m;
            }
        }
    }
}

// Compute the convolution using threads
void MaxPoolingLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void MaxPoolingLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void MaxPoolingLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML