#include "Softmax.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void SoftmaxLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    //Assumptions

    // int N = 1; // N corresponds to batch size

    // Assuming dims is setup as

    //

    // logInfo("Softmax");

    Array1D_fp32 out = this->getOutputData().getData<Array1D_fp32>();
    LayerParams p_out = this->getOutputParams();

    Array1D_fp32 in = dataIn.getData<Array1D_fp32>(); //Assuming input shape
    LayerParams p_in = dataIn.getParams();

    for(long unsigned int m = 0; m < p_out.dims[0]; m++){
        fp32 denominator = 0;

        for(long unsigned int i =0; i < p_out.dims[0]; i++){
            denominator += exp(in[i]);
        }


        out[m] = exp(in[m]) / denominator;

    }
}

// Compute the convolution using threads
void SoftmaxLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void SoftmaxLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void SoftmaxLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML