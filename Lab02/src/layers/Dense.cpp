#include "Dense.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void DenseLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    //Assumptions

    // int N = 1; // N corresponds to batch size

    // Assuming dims is setup as

    //

    // logInfo("Dense");

    Array1D_fp32 out = this->getOutputData().getData<Array1D_fp32>();
    LayerParams p_out = this->getOutputParams();

    Array1D_fp32 in = dataIn.getData<Array1D_fp32>(); //Assuming input shape

    LayerParams p_in = dataIn.getParams();

    Array2D_fp32 w = this->getWeightData().getData<Array2D_fp32>();//Assuming input shape

    LayerParams p_w = this->getWeightParams();

    Array1D_fp32 bias = this->getBiasData().getData<Array1D_fp32>();
    LayerParams p_bias = this->getBiasParams();

    for(long unsigned int m = 0; m < p_out.dims[0]; m++){

        fp32 sum = 0;
        for(long unsigned int c = 0; c < p_in.dims[0]; c++){
            sum += in[c] * w[c][m];
        }

        out[m] = sum + bias[m] < 0 ? 0 : sum + bias[m];
    }
}

// Compute the convolution using threads
void DenseLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void DenseLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void DenseLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML